package com.kryviak;

public class ApplicationCalculatorExample {

    public int getCalculatorValue() {
        CalculatorExample calculatorExample = new CalculatorExample() {
        };
        return calculatorExample.calculateTwo(4, 6);
    }

    CalcMy calcMy = new CalcMy() {

        @Override
        public int calcSeparate(int value1, int value2) {
            return value1 / value2;
        }
    };

    public int calcSeparateValue(int value1, int value2) {
        return calcMy.calcSeparate(value1, value2);
    }

    public static void main(String[] args) {
        ApplicationCalculatorExample applicationCalculatorExample = new ApplicationCalculatorExample();
        applicationCalculatorExample.calcSeparateValue(9, 3);
    }
}
