package com.kryviak;

public interface CalculatorExample {
    default int calculateTwo(int first, int last) {
        return first + last;
    }
}
