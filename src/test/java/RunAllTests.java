import com.kryviak.CalculatorMockTest;
import com.kryviak.SimpleTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({ CalculatorMockTest.class, SimpleTest.class} )
public final class RunAllTests {}

