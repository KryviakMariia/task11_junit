package com.kryviak;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static junit.framework.TestCase.*;

public class SimpleTest {
    private Sample sample = new Sample();

    @Before
    public void printMessage() {
        sample.setValue1(5);
        sample.setValue2(7);
        System.out.println("Before");

    }

    @Test
    public void test1CalcResult() {
        int value = sample.calcResult();
        assertEquals("The result is " + value, 12, value);
        assertFalse("value1 == 5", sample.getValue1() != 5);
        System.out.println("Test1");
    }

    @Test
    public void test2Name() {
//        sample.setName("Ma");
        assertNull("The field 'name' is not null", sample.getName());
        sample.setName("Ma");
        assertSame("Not the same name", sample.getName(), "Ma");
        System.out.println("Test2");
    }

    @Ignore
    @Test
    public void ignoreTest() {
        System.out.println("Test3");
    }

    @After
    public void setSampleNull() {
        sample.setName(null);
        sample.setValue1(0);
        sample.setValue2(0);
        System.out.println("After");
    }
}
