package com.kryviak;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertSame;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CalculatorMockTest {

    public static final boolean RESULT = true;
    @InjectMocks
    ApplicationCalculatorExample applicationCalculatorExample = new ApplicationCalculatorExample();

    @Mock
    CalculatorExample calculatorExample;
    CalcMy calcMy;

    @Test
    public void testCalculator() {
        when(calculatorExample.calculateTwo(2, 6)).thenReturn(8);

        assertEquals(calculatorExample.calculateTwo(2, 6), 8);
        System.out.println("Class 'CalculatorMockTest' Test1");
    }

    @Test
    public void testMyCalc() {
        assertSame(true, RESULT);
        assertEquals(applicationCalculatorExample.calcMy.calcSeparate(9, 6), 1);

    }
}
